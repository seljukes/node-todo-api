const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// Todo.remove({}).then((result) => {
//   console.log(result); 
// });

// Todo.findOneAndRemove({text: "Something to do2"}).then((todo) => {
//   console.log(todo);
// });

// Todo.findByIdAndRemove("5b83fff5c0cbc240d89316f6").then((todo) => {
//   console.log(todo);
// });